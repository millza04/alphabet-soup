from classes.reader import readInput
from classes.checker import checkGrid
from classes.writer import writeOutput

# read file
dimX, dimY, grid, answer = readInput(open("input/input4.txt", "r"))

# find answers
# result = ("word", x cord, y cord, direction)
result = checkGrid(grid, answer, dimX, dimY)

# print output
writeOutput(result, open("output/output4.txt", "w"), dimX, dimY)
