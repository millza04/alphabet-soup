def checkGrid(grid, answer, dimX, dimY):
    # For each index of grid, check if an answer is present
    countR = 0
    countD = 0
    countRD = 0
    countRB = 0
    countDB = 0
    countRDB = 0
    countLD = 0
    countLDB = 0
    result = []
    for x in range(0, dimX):
        for y in range(0, dimY):
            for a in answer:
                # Check is a occurs
                for z in range(0, len(a)):
                    # Check right
                    if ((x+z) < dimX):
                        if (grid[y][x+z] == a[z]):
                            countR += 1
                    # Check down
                    if ((y+z) < dimY):
                        if (grid[y+z][x] == a[z]):
                            countD += 1
                    # Check diagnonal
                    if ((y+z) < dimY and (x+z) < dimX):
                        if (grid[y+z][x+z] == a[z]):
                            countRD += 1
                    # Check diagnonal left
                    if ((y-z) >= 0 and (x+z) < dimX):
                        if (grid[y-z][x+z] == a[z]):
                            countLD += 1

                    # Checking if word is backwards
                    # Check right
                    if ((x+z) < dimX):
                        if (grid[y][x+z] == a[len(a) - z - 1]):
                            countRB += 1
                    # Check down
                    if ((y+z) < dimY):
                        if (grid[y+z][x] == a[len(a) - z - 1]):
                            countDB += 1
                    # Check diagonal
                    if ((y+z) < dimY and (x+z) < dimX):
                        if (grid[y+z][x+z] == a[len(a) - z - 1]):
                            countRDB += 1
                    # Check diagnonal left
                    if ((y-z) >= 0 and (x+z) < dimX):
                        if (grid[y-z][x+z] == a[len(a) - z - 1]):
                            countLDB += 1
                # If count equals its dimentsion, we found a match
                if (countR == len(a)):
                    result.append((a, x, y, "r"))
                if (countD == len(a)):
                    result.append((a, x, y, "d"))
                if (countRD == len(a)):
                    result.append((a, x, y, "rd"))
                if (countRB == len(a)):
                    result.append((a, x, y, "rb"))
                if (countDB == len(a)):
                    result.append((a, x, y, "db"))
                if (countRDB == len(a)):
                    result.append((a, x, y, "rdb"))
                if (countLD == len(a)):
                    result.append((a, x, y, "ld"))
                if (countLDB == len(a)):
                    result.append((a, x, y, "ldb"))
                # Reset vars
                countR = 0
                countD = 0
                countRD = 0
                countRB = 0
                countDB = 0
                countRDB = 0
                countLD = 0
                countLDB = 0
    return result
