from array import *
# Given the input file, returns the valuable input data
def readInput(f):
    list = []
    grid = []
    answer = []
    for line in f:
        # Load into array
        if (line != "\n"):
            list.append(line.replace("\n", ""))
    # The first line of the list is our dimensions
    dimX, dimY = list[0].split("x")
    dimX = int(dimX)
    dimY = int(dimY)
    # Using the dimensions, get only the grid
    for y in range(0, int(dimY)):
        grid.insert(y, list[y+1])
    # Get answers
    for y in range(1, (len(list)-int(dimY))):
        answer.insert(y-1, list[y+int(dimY)])
    return(dimX, dimY, grid, answer)
