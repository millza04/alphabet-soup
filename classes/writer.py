def writeOutput(result, file, dimX, dimY):
    for r in result:
        # output: HELLO 0:0 4:4
        word, x, y, direction = r
        if (direction == "r"):
            file.write(word +" "+ str(x) +":"+ str(y) + " " + str(x + len(word)- 1) + ":" + str(y) + "\n")
        if (direction == "d"):
            file.write(word +" "+ str(x) +":"+ str(y) + " " + str(x) + ":" + str(y + len(word)- 1) + "\n")
        if (direction == "rd"):
            file.write(word +" "+ str(x) +":"+ str(y) + " " + str(x + len(word)- 1) + ":" + str(y + len(word)- 1) + "\n")
        if (direction == "rb"):
            file.write(word +" "+ str(x + len(word)- 1) +":"+ str(y) + " " + str(x) + ":" + str(y) + "\n")
        if (direction == "db"):
            file.write(word +" "+ str(x) +":"+ str(y + len(word)- 1) + " " + str(x) + ":" + str(y) + "\n")
        if (direction == "rdb"):
            file.write(word +" "+ str(x + len(word)- 1) +":"+ str(y + len(word)- 1) + " " + str(x) + ":" + str(y) + "\n")
        if (direction == "ld"):
            file.write(word +" "+ str(x) +":"+ str(y) + " " + str(x + len(word)- 1) + ":" + str(y - len(word)- 1) + "\n")
        if (direction == "ldb"):
            file.write(word +" "+ str(x + len(word)- 1) +":"+ str(y - len(word)- 1) + " " + str(x) + ":" + str(y) + "\n")
